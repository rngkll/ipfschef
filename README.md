# IPFSChef

We aim to create a distributed video service, leveraging on IPFS.

HLS is used as the type of playlist(m3u8), this is because of the plain text nature and the avility to reference by address every object.

By adding every segment of the video to IPFS, then refering the segments in the m3u8 playlist, we get HLS over IPFS, this brings us to the problem of distributing the playlist, to which we decide to use IPFS messaging system called *pubsub*.

By using pubsub, the client subscribes to the pubsub and gets the master playlist.

That output can be ingested by a normal player using the an IPFS client running un localhost.

```mermaid
graph TD
subgraph ipfsChef
C[Master playlist] --> F
D[Media playlist] --> F
E[TS segment] --> F

F[ipfsChef]

F --> |Modified master playlist| G[IPFS pubsub]
F --> |Modified media playlist| H[IPFS or pubsub TBD]
F --> |Hashed Segment|I[IPFS]
end

subgraph Encoding
A[FFmpeg] -->|HLS stream| B(HLS files)
end
```

# Components

* [FFMpeg](https://ffmpeg.org/)
* [IPFS](https://ipfs.io/)
* [Grafov m3u8](https://github.com/grafov/m3u8)
* [IPFS pubsub](https://blog.ipfs.io/25-pubsub/)


# Steps
1.  Install FFmpeg

2.  Install IPFS
3.  Create stream
4.  Run ipfsChef

## Generate video in webm format

ffmpeg is needed, latest stable version is recommended.

Use the available scripts for testing:
* VOD:

* Live:

 
# Go dependencies

https://github.com/ipfs/go-ipfs#build-from-source

```
$ go get -u -d github.com/ipfs/go-ipfs

$ cd $GOPATH/src/github.com/ipfs/go-ipfs
$ make install
